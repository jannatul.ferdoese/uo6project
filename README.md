# UO6Project
Choose a self-explaining name for your project.

## Description
Todolist: A python CLI which makes an application for todo-list

## Todo List

This project includes a `todolist` file that serves as a task tracker for the project. It helps you keep track of the tasks that need to be completed, their status, and any additional notes or details related to each task.
### Usage

- To add a new task, append a new line to the file following the format mentioned above.

- To mark a task as completed, update the task status to  in the file.

- To update task details or notes, modify the respective line in the file.
- To update in another file in json .





### Installation

1. Clone the repository to your local machine.

2. Open the `todolist` file using a text editor or a compatible application.

3. Make sure you have the necessary dependencies installed if any.


## Support
Jannatul ferdoese ,email jannatul.ferdoese@chasacademy.se

## Sprint 2
Make unit test for todo list.
Share your idea how can i better in proffectional way.
If you have ideas for releases in the future, it is a good idea to list them in the README.


